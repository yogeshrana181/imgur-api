import uuid
import pickle
import flask
from flask import Flask
from base64 import b64encode
from datetime import datetime
import requests


app = Flask(__name__)        # arsing as app
access_token = '9b8c8740c6c976cb82733d2a53e849b1a32d1d13'  # Access token
client_id = '8a0c57c17b89daa'                               # Client ID


@app.route("/v1/images/upload", methods=["POST"])
def upload_images():
    img_urls = ''
    if flask.request.values:                         # values from keyword argument
        img_urls= flask.request.values['urls']
        img_urls = img_urls.split(',')
        img_urls = set(img_urls)                                          # unique urls list
    my_id = uuid.uuid1()                                 # unique session id
    my_id= str(my_id)
    all_images_uploaded_links = ''                  # variable initialized
    list_of_dict=[]
    for i in img_urls:
        storing_dict = {'url': i, 'status': False, 'imgur_url': None}  # dict for storing variable values
        list_of_dict.append(storing_dict)                       # dict list of variable values adding
    pickle.dump(list_of_dict, open(my_id+"data", "wb"))      # saving values to file by pickle method
    date_time_job_created = datetime.now().isoformat()      # converting date_time to iso format
    time = {'job_created':date_time_job_created, 'job_finished':None}   # time dict initialized
    pickle.dump(time, open(my_id+"time", "wb"))         # saving time values to file using pickle method
    for index, i in enumerate(list_of_dict):        # looping urls by enumerate
        image_url = i['url']
        f = open('demo.jpg', 'wb')          # initializing new image named demo.jpg
        f.write(requests.get(image_url).content)  # Downloading url image and saving as demo.jpg
        try:
            url = "https://api.imgur.com/3/image"
            payload = {'image': b64encode(open(str('path_of_demo.jpg'), 'rb').read()),  # encoding saving image
                       'description': image_url}
            files = {}

            headers = {
                    'Authorization': 'Client-ID ' + client_id  # add client id
            }
            response_image = requests.request('POST', url, headers=headers, data=payload, files=files, allow_redirects=False,  # Response by request upload of an image
                                        timeout=100)
            image_data = response_image.json()   # converting response to json
            image_id = image_data['data']['id']     # image Hash / ID
            image_link = image_data['data']['link']  # link of an image after upload
            if image_id:
                list_of_dict[index]['status'] = True            # Turning status as True after uploaded image
                list_of_dict[index]['imgur_url'] = image_link      # image link
                pickle.dump(list_of_dict, open(my_id+"data", "wb"))   # Saving updating list of dict
                try:
                    url = 'https://api.imgur.com/3/image/'+image_id+'/favorite'     # Adding uploaded image to fav
                    payload = {}
                    headers = {
                        'Authorization': 'Bearer ' + access_token
                    }
                    requests.request('POST', url, headers=headers, data=payload, allow_redirects=True,
                                                timeout = 100)
                except:
                    pass
            f.close()  # file close
        except:
            list_of_dict[index]['status'] = 'Fail'      # If Exception then turn status to fail
            pickle.dump(list_of_dict, open(my_id+"data", "wb"))  # saving update list of dict data
            f.close()
    data = {'jobId': my_id}
    time = {'job_created': date_time_job_created, 'job_finished': datetime.now().isoformat()} # adding job finishingtime
    pickle.dump(time, open(my_id + "time", "wb"))  # saving time data
    return flask.jsonify(data)       # Retuning session id as job id


@app.route("/v1/images/upload/<jobId>", methods=["GET"])        # API call without keyword argument
def get_images_upload(jobId):
    id = jobId              # job id returning from post api of uploading images
    pending = []
    complete = []
    failed = []
    try:
        status = 'pending'      # declaring method as current status pending
        time_data = pickle.load(open(id+"time","rb"))       # getting time data
        list_of_dict = pickle.load(open(id + "data", "rb"))   # getting images status data
        for i in list_of_dict:
            if time_data['job_finished']:   # if time completed date found
                status = 'complete'                 # then turn status as complete
            elif i['status'] == True:                    # if status as true from previous response
                status = 'in-progress'                  # then turn status shoin working or in progress

            if i['status'] == True:                                         # if status is true
                complete.append(i['imgur_url'])             # adding uploaded url of image
            elif i['status'] == False:
                pending.append(i['url'])                # adding non uploaded urls
            elif i['status']=='Fail':           # if fails
                failed.append(i['url'])                             # add to failed section
        uploaded = {'pending': pending, 'complete': complete, 'failed': failed}
        result={'id': id, 'created': time_data['job_created'], 'finished': time_data['job_finished'], 'status': status,
                'uploaded': uploaded}   # return all result
    except:
        pass
    return flask.jsonify(result)


@app.route("/v1/images/", methods=["GET"])       # API for showing all images uploaded
def get_all_images():
    user_name = 'yogeshrana'                # Your username of Imgur
    result=[]
    url = 'https://api.imgur.com/3/account/'+user_name+'/favorites/'        # url for favorites
    payload = {}
    headers = {
        'Authorization': 'Bearer '+ access_token             # access token
    }
    # get method url calling
    response = requests.request('GET', url, headers=headers, data=payload, allow_redirects=True, timeout=100)
    response = response.json()
    data = response['data']
    for i in data:
        result.append(i['link'])
    return flask.jsonify({'uploaded': result})      # getting all result of images


if __name__ == "__main__":              # Main method starts from here
    app.run(debug=True, host='0.0.0.0')       # running app